AndroidProguardFiles
==================

AndroidProguardFiles, progurd files for android java application development.

Tool Level
=========

Android Tools 11 or hgher, ADT 17 or higher

Project License
============

Apache License 2.0

Project Lead
==========

Fred Grott

blogs:

[FredGrott-posterous](http://fredgrott.posterous.com)

[Shareme-Githubpages](http://shareme.github.com)
